---
title: Konversation 1.2.3 has been released! 
date: 2010-02-12
layout: post
---
Konversation 1.2.3 is a hotfix release that improves upon an earlier fix, originally included in Konversation 1.2.2, that increases the reliability of Konversation\'s interaction with the D-Bus inter-process communication daemon.

Changes from 1.2.2 to 1.2.3:
<ul>
<li>Increased reliability of interactions with the D-Bus inter-process communication daemon.</li>
</ul>



