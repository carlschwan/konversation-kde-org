---
title: konversation 1.7.1 released! 
date: 2017-05-09
layout: post
---

Konversation v1.7.1 is a maintenance release that addresses issues with some of the new features introduced in the last major release, such as SASL EXTERNAL and server-time support, along with other minor fixes. As per usual this maintenance release also ships the latest round of translation updates. 

 Changes from Konversation 1.7 to 1.7.1: 
 
+ Fixed requesting the znc.in/server-time-iso capability from the server. 
+ Fixed SASL EXTERNAL authentication without an account name set in the Identity settings. 
+ Fixed clicking on nickname links containing the ` character. 
+ Fixed the Highlight page in the Configure Konversation dialog not enabling/disabling the Apply button correctly. 
+ Minor cleanups in application metadata setup code. 
 

