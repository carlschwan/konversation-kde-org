---
title: Konversation 1.6 released!
date: 2015-04-08
layout: post
---
Konversation 1.6 is the first stable release of Konversation built on the new KDE Frameworks 5 and Qt 5 library sets, improving integration into many desktop environments, including Plasma Desktop 5, and adding first support for hi-dpi scaling. In addition to porting and reworking things for hi-dpi scaling, a number of behavior improvements and bug fixes were implemented, particularly improved nickname selection behavior at connection time and better layout behavior for the channel topic area.<br>
Unfortunately the 1.6 release removes support for integrating with the KDE Address Book, as the interfaces Konversation was using to achieve this were dropped from KDE Frameworks. A replacement is in the works in the form of the new KPeople library, which we intend to use in a future release. Using KPeople, Konversation will then be able to tightly integrate with various contact management-related features in Plasma Desktop.

Changes from 1.6-beta1 to 1.6:
 
+ Improved support for character set aliases via KCharsets. 
+ Improved behavior when reconnecting and Konversation had to use an alternate nickname for the previous connection because the preferred nickname was already in use: Konversation will now try harder to go back to nicknames sorted earlier in the identity's nickname list, instead of advancing down the list without checking whether earlier nicknames have become available again. 
+ Konversation will no longer give up trying to connect when only a single nickname is configured and not available at connection time. Instead, it will do as many reconnection attempts as allowed by user configuration. 
+ Fixed incorrect size calculations for the channel topic text label causing cut-off text and unwanted collapsing of the topic area. 
+ Fixed a bug causing auto-connect not to connect to networks in the order they are listed in the Server List dialog. 
+ Fixed a bug causing the main window not to be raised when an Konversation is started an additional time and the window is currently minimized. 
+ The default tabs position is now 'Left', i.e. the treelist version of the tab bar. 
+ Tabs can now be reordered by drag and drop again. 
+ Fixed various rendering issues in the treelist version of the tab bar and made it high-DPI scaling-capable. 
+ Fixed a bug causing Channel List tabs not to be sorted below the status tab of the server they belong to in the treelist version of the tab bar, and corrupt its contents. 
+ Fixed margins in the Edit Network dialog. 
+ Improved wording and correctness of several interface messages. 
+ Fixed a bug causing some interface messages and labels not to use their translated versions when running with a language other than US English. 
+ Fixed a bug causing Konversation not to generate default command aliases for installed scripts at startup anymore. 
+ The bundled 'cmd' script now defaults to trying to decode command output as Utf-8 even when run on Python 2. 
+ Fixed the bundled 'bug' script not working. 
+ Fixed naming of bundled icons. 
+ Konversation now opts into Qt's high-DPI pixmap handling. 
+ Adjusted build system to use co-installable version of qca-qt5. 
+ Fixed build failures on Windows. 
 

