<?php
// Links definition
$links = array(
"Home" => $config['directory']."/",
"About the Project" => $config['directory']."/?about",
"Handbook" => "http://docs.kde.org/development/en/extragear-network/konversation/index.html",
"Report Bugs" => "http://bugs.kde.org/",
"Screenshots" => $config['directory']."/?screenshots",
"Wiki" => "http://userbase.kde.org/Konversation",
"Contact" => $config['directory']."/?contact",
"Authors" => $config['directory']."/?authors",
"Changelog" => "https://quickgit.kde.org/?p=konversation.git",
"Browse Git" => "http://projects.kde.org/projects/extragear/network/konversation/repository",
"Get from Git" => "http://konversation.kde.org/wiki/Sources"
);
// Download definition
$download = array(
"Sources" => "http://download.kde.org/download.php?url=stable/konversation/1.7.5/src/konversation-1.7.5.tar.xz",
"ArchLinux" => "http://userbase.kde.org/Konversation/Distributions/ArchLinux",
"ArkLinux" => "http://userbase.kde.org/Konversation/Distributions/ArkLinux",
"Debian GNU/Linux" => "http://packages.debian.org/konversation",
"Fedora" => "http://koji.fedoraproject.org/koji/packageinfo?packageID=2237",
"FreeBSD" => "http://www.freshports.org/irc/konversation-kde4",
"Gentoo" => "http://userbase.kde.org/Konversation/Distributions/Gentoo",
"Kubuntu" => "http://userbase.kde.org/Konversation/Distributions/Kubuntu",
"SuSE Linux" => "http://software.opensuse.org/search?p=1&amp;q=konversation",
"Slackware" => "http://userbase.kde.org/Konversation/Distributions/Slackware",
"NetBSD" => "http://www.netbsd.org/Documentation/software/packages.html",
"PC-BSD" => "http://pbidir.com/bt/pbi/241",
"Windows" => "https://binary-factory.kde.org/job/Konversation_Nightly_win64/"
);
?>
