<ul>
<li>Dario Abatianni<br />
Original Author, Project Founder
</li>
<li>
Peter Simonsson<br />
Maintainer
</li>
<li>
Eike Hein<br />
Maintainer, Release Manager, User interface, Protocol handling
</li>
<li>
Shintaro Matsuoka<br />
DCC, Encoding handling, OSD positioning
</li>
<li>
Eli MacKenzie<br />
Protocol handling, Input line
</li>
<li>
İsmail Dönmez<br />
Blowfish Support, SSL Support, Ported to KNetwork, Colored Nicknames, Icon Theme Support
</li>
<li>
John Tapsell<br />
Refactoring, KAddressBook/Kontact integration
</li>
</ul>
<ul>
<li style="border-top:1px solid #5A799D">
Christian Muehlhaeuser<br />
Multiple modes extension, Close widget placement, OSD functionality
</li>
<li>
Gary Cramblitt<br />
Documentation, Watched Nicks Online improvements, Custom web browser extension
</li>
<li>
Matthias Gierlings<br />
Color configurator, Highlight dialog
</li>
<li>
Alex Zepeda<br />
DCOP interface
</li>
<li>
Stanislav Karchebny<br />
Non-Latin1-Encodings
</li>
<li>
Mickael Marchand<br />
Konsole part view
</li>
<li>
Michael Goettsche<br />
Quick connect, Ported new OSD, other features and bugfixes
</li>
<li>
Benjamin Meyer<br />
A Handful of fixes and code cleanup
</li>
<li>
Jakub Stachowski<br />
Drag&amp;Drop improvements
</li>
<li>
Sebastian Sariego<br />
Artwork
</li>
<li>
Olivier Bédard<br />
www.konversation.org hosting
</li>
<li>
Renchi Raju<br />
Firefox style searchbar
</li>
<li>
Michael Kreitzer<br />
Raw modes, Tab grouping per server, Ban list
</li>
<li>
Frauke Oster<br />
System tray patch
</li>
<li>
Lucijan Busch<br />
Bug fixes
</li>
<li>
Sascha Cunz<br />
Extended user modes patch
</li>
<li>
Steve Wollkind<br />
Close visible tab with shortcut patch
</li>
<li>
Thomas Nagy<br />
Cycle tabs with mouse scroll wheel
</li>
<li>
Tobias Olry<br />
Channel ownership mode patch
</li>
<li>
Ruud Nabben<br />
Option to enable IRC color filtering
</li>
</ul>