<p>Konversation is a user-friendly Internet Relay Chat (IRC) client built on the KDE Platform.</p>
<p>Features</p>
<ul>
<li>Standard IRC features</li>
<li>SSL server support</li>
<li>Bookmarking support</li>
<li>Easy to use graphical user interface</li>
<li>Multiple servers and channels in one single window</li>
<li>DCC file transfer</li>
<li>Multiple identities for different servers</li>
<li>Text decorations and colors</li>
<li>OnScreen Display for notifications</li>
<li>Automatic UTF-8 detection</li>
<li>Per channel encoding support</li>
<li>Theme support for nick icons</li>
<li>Highly configurable</li>
</ul>